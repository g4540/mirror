using System.Runtime.CompilerServices;
using System.Globalization;
using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class Button : MonoBehaviour, IInteractable
{
    [SerializeField]
    GameObject targetObject;
    IInteractable targetInteractable;

    [SerializeField]
    Sprite buttonActive;

    [SerializeField]
    Sprite buttonInactive;

    [SerializeField]
    SpriteRenderer buttonRenderer;

    [SerializeField]
    private UnityAction action;

    [SerializeField]
    private float CooldownTime = 0.5f;

    private float cooldownTimer = 0.0f;

    public bool SingleUse = false;
    public bool WasUsed = false;

    public void Start()
    {
        buttonRenderer.sprite = buttonInactive;
        targetInteractable = targetObject.GetComponent<IInteractable>();
    }

    public void Update()
    {
        if (cooldownTimer > 0.0f || WasUsed)
        {
            cooldownTimer -= Time.deltaTime;
            buttonRenderer.sprite = buttonActive;
        }
        else
        {
            buttonRenderer.sprite = buttonInactive;
        }
    }

    public void Interact()
    {
        if ((SingleUse && !WasUsed) || !SingleUse && cooldownTimer <= 0)
        {
            cooldownTimer = CooldownTime;
            WasUsed = true;

            if (action != null)
                action.Invoke();

            //Added simple call to other interactables to aid in level scripting
            targetInteractable.Interact();
        }


    }
}