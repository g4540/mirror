using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField]
    public bool horizontalOnly = false;
    private float _holdY;
    [SerializeField]
    public bool verticalOnly = false;
    private float _holdX;
    [SerializeField]
    public bool moving;
    [SerializeField]
    public GameObject[] destinations;
    [SerializeField]
    public float speed = 1f;
    [SerializeField]
    public bool circular = false;
    [SerializeField]
    public float cooldownDuration = 2f;

    private Vector2 _destination;
    private int _currentIndex = 0;
    private bool _accending = true;
    private bool _docked = true;
    private bool _cooldown = true;
    private float _currentCooldown;
    private bool _reachedDestination = false;
    private bool _right;
    private bool _up;

    //Raycasts for handling Rigidbody2D Kinematic collision
    // void FixedUpdate()
    // {
    //     //Check raycast width and height
    //     float raycastWidth = transform.gameObject.GetComponent<SpriteRenderer>().bounds.size.x / 2;
    //     float raycastHeight = transform.gameObject.GetComponent<SpriteRenderer>().bounds.size.y / 2;

    //     //check which direction the platform is heading based on the _destination.
    //     //Toggle Raycasts on and off based on the results.
    //     if (transform.position.x < _destination.x)
    //     {
    //         _right = true;
    //     }
    //     else
    //     {
    //         _right = false;
    //     }

    //     if (transform.position.y < _destination.y)
    //     {
    //         _up = true;
    //     }
    //     else
    //     {
    //         _up = false;
    //     }

    //     #region Raycast Down
    //     // Cast a ray straight down.
    //     if (!_up)
    //     {
    //         RaycastHit2D hitDown = Physics2D.Raycast(transform.position, Vector2.down, raycastHeight);
    //         Debug.DrawRay(transform.position, Vector2.down, Color.green);

    //         if (hitDown.collider != null)
    //         {
    //             if (hitDown.transform.gameObject == destinations[_currentIndex])
    //             {
    //                 _reachedDestination = true;
    //             }
    //         }
    //     }
    //     #endregion

    //     #region Raycast Up
    //     if (_up)
    //     {
    //         //Cast a ray straight up.
    //         RaycastHit2D hitUp = Physics2D.Raycast(transform.position, Vector2.up, raycastHeight);
    //         Debug.DrawRay(transform.position, Vector2.up, Color.green);

    //         if (hitUp.collider != null)
    //         {
    //             if (hitUp.transform.gameObject == destinations[_currentIndex])
    //             {
    //                 _reachedDestination = true;
    //             }
    //         }
    //     }
    //     #endregion

    //     #region Raycast Left
    //     if (!_right)
    //     {
    //         //Cast a ray to the left.
    //         RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, Vector2.left, raycastWidth);
    //         Debug.DrawRay(transform.position, Vector2.left, Color.green);

    //         //Check if we reached our destination
    //         if (hitLeft.collider != null)
    //         {
    //             if (hitLeft.transform.gameObject == destinations[_currentIndex])
    //             {
    //                 _reachedDestination = true;
    //             }
    //         }
    //     }
    //     #endregion

    //     #region Raycast Right
    //     if (_right)
    //     {
    //         //Cast a ray to the right.
    //         RaycastHit2D hitRight = Physics2D.Raycast(transform.position, Vector2.right, raycastWidth);
    //         Debug.DrawRay(transform.position, Vector2.right, Color.green);

    //         if (hitRight.collider != null)
    //         {
    //             if (hitRight.transform.gameObject == destinations[_currentIndex])
    //             {
    //                 _reachedDestination = true;
    //             }
    //         }
    //     }
    //     #endregion
    // }

    void Update()
    {
        //Referenced https://www.youtube.com/watch?v=UlEE6wjWuCY&ab_channel=CodinginFlow
        _destination = destinations[_currentIndex].transform.position;

        if (moving && _cooldown)
        {
            CoolDown();
        }

        //Only move if Moving is true. Otherwise remain still. Dock if not docked and moving is false
        if (moving && !_cooldown || !moving && !_docked)
        {
            _holdY = transform.position.y;
            _holdX = transform.position.x;

            //If Horizontal Platform should move left and right
            if (horizontalOnly && !verticalOnly)
            {
                _destination.y = _holdY;

                //Check if unreachable
                if (transform.position.x <= _destination.x + 0.1f
                    && transform.position.x >= _destination.x - 0.1f)
                {
                    //Debug.Log("horizontalOnly unreachable");
                    InPositionBackup();
                }
            }

            //If !Horizontal Platform should move up and down
            else if (verticalOnly && !horizontalOnly)
            {
                _destination.x = _holdX;

                //Check if unreachable
                if (transform.position.y <= _destination.y + 0.1f
                    && transform.position.y >= _destination.y - 0.1f)
                {
                    //Debug.Log("verticalOnly unreachable");
                    InPositionBackup();
                }
            }

            else if (verticalOnly && horizontalOnly)
            {
                verticalOnly = false;
                horizontalOnly = false;
                throw new Exception("HorizontalOnly and VerticalOnly were both set to true. Options will be turned off.");
            }

            transform.position = Vector2.Lerp(transform.position, _destination, Time.deltaTime * speed);

            if (_reachedDestination)
            {
                InPositionBackup();
            }
        }
    }

    /* //Now that kinematic is implemented this appears unneeded
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor" && destinations[_currentIndex] == collision.gameObject
            || collision.gameObject.tag == "Wall" && destinations[_currentIndex] == collision.gameObject)
        {
            //only adjust _accending if the platform is not moving in a circular pattern
            if (!circular)
            {
                if (_currentIndex >= destinations.Length - 1)
                {
                    _accending = false;
                }
                else if (_currentIndex - 1 < 0)
                {
                    _accending = true;
                }
            }
            _docked = true;

            NextDestination();
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        _docked = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Waypoint" && destinations[_currentIndex] == collision.gameObject)
        {
            //only adjust _accending if the platform is not moving in a circular pattern
            if (!circular)
            {
                if (_currentIndex >= destinations.Length - 1)
                {
                    _accending = false;
                }
                else if (_currentIndex - 1 < 0)
                {
                    _accending = true;
                }
            }
            _docked = true;

            NextDestination();
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        _docked = false;
    }
    */

    void NextDestination()
    {
        if (cooldownDuration > 0)
        {
            _cooldown = true;
        }

        if (_accending)
        {
            //Take into consideration if the platform is moving in a circular pattern
            if (_currentIndex >= destinations.Length - 1)
            {
                _currentIndex = 0;
            }
            else
            {
                _currentIndex++;
            }
        }
        else
        {
            _currentIndex--;
        }
    }

    void CoolDown()
    {
        _currentCooldown -= Time.deltaTime;

        if (_currentCooldown <= 0)
        {
            _cooldown = false;
            _currentCooldown = cooldownDuration;
        }
    }

    void InPositionBackup()
    {
        _reachedDestination = false;
        //only adjust _accending if the platform is not moving in a circular pattern
        if (!circular)
        {
            if (_currentIndex >= destinations.Length - 1)
            {
                _accending = false;
            }
            else if (_currentIndex - 1 < 0)
            {
                _accending = true;
            }
        }
        _docked = true;

        NextDestination();
    }
}
