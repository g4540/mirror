using UnityEngine;

/// <summary>
/// Holds any global state we may need such as game settings
/// </summary>
public class Globals : MonoBehaviour
{

    public static Globals Instance { get; private set; }

    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
        }
        DontDestroyOnLoad(this);
        Instance = this;
    }
}