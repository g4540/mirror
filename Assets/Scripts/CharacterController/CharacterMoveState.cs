using System;

public enum MoveState
{
    None,
    Idle,
    Jump,
    Walk,
    Thrust,
    Climb,
}

[Flags]
public enum DirectionState
{
    None = 0,
    Up = 1,
    Down = 2,
    Left = 4,
    Right = 8,
}