using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class AddForceXZone : MonoBehaviour
{
    public bool playerInWindZone;
    public Rigidbody2D rigidbody2D;

    public float speedZone =.3f; 

    // Update is called once per frame
    void Update()
    {
        if (playerInWindZone)
        {
            rigidbody2D.AddForce(transform.right * speedZone);
            Debug.Log("Trigger Enter ADD FORCE");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "Player")
        {
            // collision.gameObject.transform.SetParent(this.transform);
            playerInWindZone = true;
            rigidbody2D = collision.gameObject.GetComponent<Rigidbody2D>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //collision.gameObject.transform.SetParent(null);
            playerInWindZone = false;
            rigidbody2D = null;
        }
    }
}
