using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class Switch : MonoBehaviour, IInteractable
{
    [SerializeField]
    Sprite switchOn;

    [SerializeField]
    Sprite switchOff;

    [SerializeField]
    SpriteRenderer switchRenderer;

    [SerializeField]
    private float CooldownTime = 0.5f;
    private float cooldownTimer = 0.0f;

    public bool SwitchIsOn = true;

    public void Start()
    {
        switchRenderer.sprite = switchOff;
    }

    public void Update()
    {
        if (cooldownTimer > 0.0f)
        {
            cooldownTimer -= Time.deltaTime;
        }
    }

    public void Interact()
    {
        if (cooldownTimer <= 0)
        {
            cooldownTimer = CooldownTime;
            SwitchIsOn = !SwitchIsOn;
        }
        switchRenderer.sprite = SwitchIsOn ? switchOn : switchOff;
    }
}