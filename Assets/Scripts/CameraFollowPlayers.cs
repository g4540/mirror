using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayers : MonoBehaviour
{
    GameObject[] Players;
    GameObject Robot;

    Transform t;


    // Start is called before the first frame update
    void Start()
    {
        Players = GameObject.FindGameObjectsWithTag("Player");
        t = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        t.position = GetMeanPosition();
    }

    private Vector3 GetMeanPosition()
    {
        Vector3 positionTotal = new Vector3(0f, 0f, -10f);

        foreach (GameObject Player in Players) {
            positionTotal += Player.GetComponent<Transform>().position;
        }
        return (positionTotal / Players.Length);
    }
}