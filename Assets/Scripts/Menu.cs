using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using TMPro;

public class Menu : MonoBehaviour
{
    [SerializeField]
    private GameObject[] buttons;
    private TextMeshProUGUI sceneName;

    void Start()
    {
        sceneName = new TextMeshProUGUI();
        sceneName.text = "";
    }

    public void OnPlay()
    {
        // TODO - We could have a level select screen as you unlock levels if we have multiple levels
        SceneManager.LoadScene("SceneSelect");
    }

    public void OnSceneSelect(GameObject selected)
    {
        Debug.Log(sceneName);
        switch(selected.GetComponent<TextMeshProUGUI>().text)
        {
            case "1":
                sceneName.text = "CLM_Level1";
                break;
            case "2":
                sceneName.text = "CLM_Level2";
                break;
            case "3":
                sceneName.text = "CLM_Level3";
                break;
            //TODO: Add new scenes here as necessary
            default:
                throw new Exception("Scene does not exist.");
                break;
        }

            if (sceneName != null || sceneName.text != "")
        {
            SceneManager.LoadScene(sceneName.text);
        }
    }

    public void OnExit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
