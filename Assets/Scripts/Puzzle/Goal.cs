using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public int playerNumber;

    // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Player "+playerNumber.ToString() + " won");
            PuzzleSystem.Instance.PlayerAtFlag(playerNumber,true);//Set player goal to completed

        }

       
    }

    // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Player Lost");
            PuzzleSystem.Instance.PlayerAtFlag(playerNumber, false);//Set player goal to completed
        }
    }
}
