using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpriteAnimation", order = 1)]
public class SpriteSet : ScriptableObject
{
    public float AnimationFramesPerSecond;
    public Sprite[] sprites;
}