using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class FrameAnimator : MonoBehaviour
{
    [SerializeField]
    private SpriteSet startingSpriteSet;

    private SpriteSet currentSpriteSet;
    private bool currentIsFlipped = false;

    private SpriteSet queuedSpriteSet;
    private bool queuedIsFlipped = false;

    private SpriteRenderer spriteRenderer;

    [SerializeField]
    public GameObject SoundManager;

    private int currentFrame = 0;
    private float timePerFrame = 0.0f;
    private float timeToNextFrame = 0.0f;
    private bool swapNextFrame = false;

    /// <summary>
    /// Set the next animation to start after the active one completes
    /// </summary>
    /// <param name="newAnimation">the new animation to use</param>
    /// <param name="flip">flip sprites</param>
    public void SwapSpriteSet(SpriteSet newAnimation, bool flip = false)
    {
        queuedSpriteSet = newAnimation;
        queuedIsFlipped = flip;
    }

    /// <summary>
    /// Immediately swap animations after the current frame is completed.
    /// </summary>
    /// <param name="newAnimation">the new animation to use</param>
    /// <param name="flip">flip sprites</param>
    public void SwapSpriteSetImmediate(SpriteSet newAnimation, bool flip = false)
    {
        currentIsFlipped = flip;

        // if an animation is already set or is null ignore
        if (newAnimation == currentSpriteSet || newAnimation == null)
            return;

        timePerFrame = 1 / newAnimation.AnimationFramesPerSecond;
        currentSpriteSet = newAnimation;
        swapNextFrame = true;
    }

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        SwapSpriteSetImmediate(startingSpriteSet, currentIsFlipped);
    }

    private void UpdateSprite()
    {
        spriteRenderer.sprite = currentSpriteSet.sprites[currentFrame];
        spriteRenderer.flipX = currentIsFlipped;
    }

    public void ResetAnimationState()
    {
        currentFrame = 0;
        queuedSpriteSet = null;
        timeToNextFrame = 0.0f;
        swapNextFrame = false;
    }

    public void Update()
    {
        timeToNextFrame -= Time.deltaTime;
        if (timeToNextFrame <= 0.0f)
        {
            if (swapNextFrame)
            {
                ResetAnimationState();
            }

            if (currentFrame == currentSpriteSet.sprites.Length - 1)
            {
                currentFrame = 0;

                if (queuedSpriteSet != null)
                {
                    SwapSpriteSetImmediate(queuedSpriteSet, queuedIsFlipped);
                    ResetAnimationState();
                }
            }
            timeToNextFrame += timePerFrame;
            currentFrame++;
            UpdateSprite();
        }

    }

}