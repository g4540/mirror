using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PuzzleSystem : MonoBehaviour
{
    public static PuzzleSystem Instance;

    public bool puzzleNumber;
    public bool player1;
    public bool player2;

    private void Awake()
    {
        // If Instance is not null (any time after the first time)
        // AND
        // If Instance is not 'this' (after the first time)
        if (Instance != null && Instance != this)
        {
            // ...then destroy the game object this script component is attached to.
            Destroy(gameObject);
        }
        else
        {
            // Tell Unity NOT to destory the GameObject this
            //  is attached to between scenes.
            DontDestroyOnLoad(gameObject);
            // Save an internal reference to the first instance of this class
            Instance = this;
        }
    }

    //This can porbably move to the Manager
    private void LateUpdate()
    {
        Completed();//This can porbably move to the Manager
    }

    private void Completed()
    {
        if (player1 && player2)
        {
            puzzleNumber = true;
            Debug.Log("Puzzle Done Done");
            SceneManager.LoadScene("Credits");
            player1 = false;
            player2 = false;
        }

        if (!player1 || !player2)
        {
            puzzleNumber = true;
            Debug.Log("Player not finished");

        }
    }

    public void PlayerAtFlag(int playerNumber, bool status)
    {
        if (playerNumber==1)
        {
            player1 = status;
        }

        if (playerNumber == 2)
        {
            player2 = status;
        }
    }
}
