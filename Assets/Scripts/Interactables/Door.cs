using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class Door : MonoBehaviour, IInteractable
{
    [SerializeField]
    Sprite doorOpenSprite;

    [SerializeField]
    Sprite doorClosedSprite;

    [SerializeField]
    Collider2D doorCollider;

    [SerializeField]
    SpriteRenderer doorRenderer;

    public bool doorOpen = false;

    public void Start()
    {
        doorCollider.isTrigger = doorOpen;
    }

    public void Update()
    {
        doorRenderer.sprite = doorOpen ? doorOpenSprite : doorClosedSprite;
    }

    public void Interact()
    {
        doorOpen = !doorOpen;

        doorCollider.isTrigger = doorOpen; // if door is open act as trigger if closed block player - CLM: Fixed inverted bool
    }
}