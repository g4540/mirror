using UnityEngine;

[RequireComponent(typeof(CharacterController), typeof(FrameAnimator))]
public class CharacterAnimator : MonoBehaviour
{
    private CharacterController controller;
    private FrameAnimator animator;

    [SerializeField]
    private SpriteSet walkAnimation;

    [SerializeField]
    private SpriteSet idleAnimation;

    [SerializeField]
    private SpriteSet jumpAnimation;

    [SerializeField]
    private SpriteSet climbAnimation;

    [SerializeField]
    private SpriteSet thrustAnimation;

    void Awake()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<FrameAnimator>();
    }

    SpriteSet StateToSpriteSet(MoveState state)
    {
        switch (state)
        {
            case MoveState.Jump:
                return jumpAnimation;
            case MoveState.Walk:
                return walkAnimation;
            case MoveState.Idle:
                return idleAnimation;
            case MoveState.Climb:
                return climbAnimation;
            case MoveState.Thrust:
                return thrustAnimation;
            default:
                return idleAnimation;
        }
    }

    void Update()
    {
        bool flip = controller.PlayerDirection == DirectionState.Left;

        animator.SwapSpriteSetImmediate(StateToSpriteSet(controller.MoveState), flip);
    }
}