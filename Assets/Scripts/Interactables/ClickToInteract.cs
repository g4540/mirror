using UnityEngine;

public class ClickToInteract : MonoBehaviour
{
    IInteractable currentInteractable = null;
    private void Update()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z += Camera.main.nearClipPlane;

        transform.position = mousePosition;

        if (Input.GetMouseButtonDown(0) && currentInteractable != null)
        {
            Debug.Log("click");
            currentInteractable.Interact();
        }
    }

    // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
    void OnTriggerEnter2D(Collider2D col)
    {
        currentInteractable = col.gameObject.GetComponent<IInteractable>();
    }

    // when the GameObjects collider arrange for this GameObject to travel to the left of the screen
    void OnTriggerExit2D(Collider2D col)
    {
        currentInteractable = null;
    }
}