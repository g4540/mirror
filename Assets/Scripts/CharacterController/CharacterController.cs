using System.Net;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterController : MonoBehaviour
{
    public MoveState MoveState;// { get; private set; }
    public DirectionState PlayerDirection;// { get; private set; }

    private IInteractable interactableObj;

    [SerializeField]
    private bool JumpEnabled;

    [SerializeField]
    private bool ThrustEnabled;

    [SerializeField]
    private bool ClimbEnabled;

    [SerializeField]
    private bool InteractEnabled;

    public bool CanJump => JumpPossible && JumpEnabled && OnFloor;
    public bool CanClimb => ClimbPossible && ClimbEnabled;
    public bool CanInteract => InteractPossible && InteractEnabled;
    public bool CanThrust => ThrustPossible && ThrustEnabled;

    private bool ClimbPossible = false;
    private bool JumpPossible = false;
    private bool ThrustPossible = false;
    private bool InteractPossible = false;
    private bool OnFloor = false;

    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float jumpSpeed;

    [SerializeField]
    private float groundRaycastDistance = 0.52f;

    private Rigidbody2D rigidBody;
    private Collider2D collider;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
    }

    void UpdateKeyboardInputs()
    {
        MoveState = MoveState.None;
        PlayerDirection = DirectionState.None;

        //BaseMovement
        if (Input.GetKey(KeyCode.A))
            PlayerDirection |= DirectionState.Left;
        else if (Input.GetKey(KeyCode.D))
            PlayerDirection |= DirectionState.Right;
        else if (Input.GetKey(KeyCode.W))
            PlayerDirection |= DirectionState.Up;
        else if (Input.GetKey(KeyCode.S))
            PlayerDirection |= DirectionState.Down;

        //Jumping
        if (Input.GetKeyDown(KeyCode.Space) && CanJump)
        {
            MoveState = MoveState.Jump;
        }

        if (Input.GetKey(KeyCode.W) && CanThrust)
        {
            MoveState = MoveState.Thrust;
        }

        //Interaction
        if (Input.GetKey(KeyCode.F) && CanInteract)
        {
            interactableObj.Interact();
        }

        //Exit
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
    }

    void Update()
    {
        UpdateKeyboardInputs();
        JumpPossible = true;
        ThrustPossible = true;

        if (CanJump && MoveState == MoveState.Jump)
        {
            Jump();
        }
        else if (CanThrust && MoveState == MoveState.Thrust)
        {
            Thrust();
        }
        else if (CanClimb)
        {
            if ((PlayerDirection & (DirectionState.Left | DirectionState.Right)) != 0)
            {
                MoveState = MoveState.Walk;
            }
            else if ((PlayerDirection & (DirectionState.Up | DirectionState.Down)) != 0)
            {
                ThrustPossible = false;
                JumpPossible = false;
                MoveState = MoveState.Climb;
            }
        }
        else if ((PlayerDirection & (DirectionState.Left | DirectionState.Right)) != 0)
        {
            MoveState = MoveState.Walk;
        }
        else
        {
            MoveState = MoveState.Idle;
        }
    }

    //Check if the player is grounded before performing the jump
    private void Jump()
    {
        rigidBody.AddForce(new Vector2(0, jumpSpeed), ForceMode2D.Impulse);
    }

    private void Thrust()
    {
        rigidBody.AddForce(new Vector2(0, jumpSpeed), ForceMode2D.Impulse);
        // Clamp max flight speed
        Vector2 vel = rigidBody.velocity;
        vel.y = Mathf.Min(vel.y, 4);
        rigidBody.velocity = vel;
    }

    Vector3 GetStateToInputVector(DirectionState allowedDirections, float moveSpeed, float yVelocity = 0.0f)
    {
        Vector3 vector = default;

        var dir = PlayerDirection & allowedDirections;

        if ((dir & DirectionState.Up) != 0)
            vector.y = 1 * moveSpeed;
        else if ((dir & DirectionState.Down) != 0)
            vector.y = -1 * moveSpeed;
        else
            vector.y = yVelocity;

        if ((dir & DirectionState.Right) != 0)
            vector.x = 1 * moveSpeed;
        else if ((dir & DirectionState.Left) != 0)
            vector.x = -1 * moveSpeed;
        else
            vector.x = 0;

        return vector;
    }

    DirectionState climbDirections = DirectionState.Up | DirectionState.Down;
    DirectionState walkDirections = DirectionState.Left | DirectionState.Right;
    DirectionState thrustDirections = DirectionState.Left | DirectionState.Right | DirectionState.Up | DirectionState.Down;
    DirectionState jumpDirections = DirectionState.Left | DirectionState.Right;

    private DirectionState GetCurrentActiveDirections()
    {
        switch (MoveState)
        {
            case MoveState.Climb:
                return climbDirections;

            case MoveState.Idle:
            case MoveState.Walk:
                return walkDirections;

            case MoveState.Thrust:
                return thrustDirections;

            case MoveState.Jump:
                return jumpDirections;
        }
        return DirectionState.None;
    }

    void FixedUpdate()
    {
        DirectionState allowedDirections = GetCurrentActiveDirections();
        float yVel = MoveState == MoveState.Climb ? 0.0f : rigidBody.velocity.y;
        rigidBody.velocity = GetStateToInputVector(allowedDirections, moveSpeed, yVel);

        // Debug.DrawRay(gameObject.transform.position, Vector3.down * 0.6f);
        var hit = Physics2D.Raycast(gameObject.transform.position, Vector3.down, groundRaycastDistance);

        if (hit.collider != null && (hit.collider.CompareTag("Floor") || hit.collider.CompareTag("MovingPlatform")))
        {
            //Debug.DrawRay(gameObject.transform.position, Vector3.down * groundRaycastDistance, Color.green);
            OnFloor = true;
        }
        else
        {
            OnFloor = false;
            //Debug.DrawRay(gameObject.transform.position, Vector3.down * groundRaycastDistance, Color.red);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Ladders
        if (other.gameObject.CompareTag("Ladder"))
        {
            Debug.Log("Entered Ladder Zone");
            ClimbPossible = true;
        }

        //Interactables
        if (other.gameObject.TryGetComponent(out IInteractable iObj))
        {
            interactableObj = iObj;
            InteractPossible = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Ladders
        if (other.gameObject.CompareTag("Ladder"))
        {
            Debug.Log("Exited Ladder Zone");
            ClimbPossible = false;
        }

        //Interactables
        if (other.gameObject.TryGetComponent(out IInteractable iObj))
        {
            interactableObj = null;
            InteractPossible = false;
        }
    }

}